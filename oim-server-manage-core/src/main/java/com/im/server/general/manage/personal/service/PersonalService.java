package com.im.server.general.manage.personal.service;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.im.server.general.common.bean.User;
import com.im.server.general.common.dao.UserDAO;
import com.onlyxiahui.common.message.result.ResultMessage;

/**
 * date 2018-06-13 10:27:23<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@Service
public class PersonalService {

	@Resource
	UserDAO userDAO;

	public User getUser(String id) {
		id=null==id?"":id;
		User user = userDAO.get(id);
		return user;
	}

	public ResultMessage updatePassword(String id, String newPassword, String oldPassword) {
		ResultMessage rm = new ResultMessage();
		try {
			if (StringUtils.isNotBlank(id) && StringUtils.isNotBlank(newPassword)) {
				User user = userDAO.get(id);
				if (null != user) {
					if (user.getPassword().equals(oldPassword)) {
						userDAO.updatePassword(id, newPassword);
					} else {
						rm.addWarning("003", "原密码错误！");
					}
				} else {
					rm.addWarning("002", "修改失败！");
				}
			} else {
				rm.addWarning("001", "修改失败！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}
}
