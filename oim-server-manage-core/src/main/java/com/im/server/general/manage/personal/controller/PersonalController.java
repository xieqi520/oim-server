package com.im.server.general.manage.personal.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.im.server.general.common.app.UserContext;
import com.im.server.general.common.bean.User;
import com.im.server.general.manage.common.annotation.PermissionMapping;
import com.im.server.general.manage.personal.service.PersonalService;
import com.onlyxiahui.common.message.result.ResultMessage;
import com.onlyxiahui.general.annotation.parameter.Define;
import com.onlyxiahui.general.annotation.parameter.RequestParameter;

/**
 * date 2018-06-04 14:59:44<br>
 * description个人中心
 * 
 * @author XiaHui<br>
 * @since
 */
@RestController
@RequestMapping("/manage")
public class PersonalController {

	@Resource
	PersonalService personalService;

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "个人信息", superKey = "personal", isIntercept = false)
	@RequestMapping(method = RequestMethod.POST, value = "/personal/info")
	public Object user(
			HttpServletRequest request,
			@Define("id") String id,
			UserContext userContext) {
		ResultMessage rm = new ResultMessage();
		User user = personalService.getUser(userContext.getCurrentUserId());
		rm.put("user", user);
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "修改密码", superKey = "personal")
	@RequestMapping(method = RequestMethod.POST, value = "/personal/updatePassword")
	public Object updatePassword(
			HttpServletRequest request,
			@Define("id") String id,
			@Define("newPassword") String newPassword,
			@Define("oldPassword") String oldPassword,
			UserContext userContext) {
		ResultMessage rm = personalService.updatePassword(userContext.getCurrentUserId(), newPassword, oldPassword);
		return rm;
	}
}
