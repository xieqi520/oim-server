package com.im.server.general.common.util;

import java.util.Map;
import java.util.Set;

import com.im.server.general.common.bean.User;
import com.onlyxiahui.query.hibernate.QueryWrapper;
import com.onlyxiahui.query.hibernate.util.QueryUtil;

public class UpdateSQLUtil {

	public static String getUpdateSQL(String tableName,String idName,QueryWrapper qw){
		
		
		StringBuilder sql = new StringBuilder();
		Map<String, Object> map =(null==qw)?null: qw.getParameterMap();
		if(map!=null&&!map.isEmpty()){
			Set<String> keySet=map.keySet();
			sql.append(" update ");
			sql.append(tableName);
			
			sql.append(" set ");
			int i=0;
			//int size=keySet.size();
			for(String key:keySet){
				i++;
				if(i==1){
					sql.append(key);
					sql.append(" = ");
					sql.append(":");
					sql.append(key);
				}else{
					sql.append(",");
					sql.append(key);
					sql.append(" = ");
					sql.append(":");
					sql.append(key);
				}
			}
			sql.append(" set ");
			
			sql.append(" where ");
			sql.append(idName);
			sql.append(" = ");
			sql.append(":");
			sql.append(idName);
		}
		return sql.toString();
	}
	
	
	public static void main(String[] arg){
		User u=new User();
		u.setAccount("100086");
		
		System.out.println(UpdateSQLUtil.getUpdateSQL("im_user", "id", QueryUtil.getQueryWrapper(u)));
	}
}
