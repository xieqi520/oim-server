import * as base from './base';
let dictionary = {};

const map = new base.Map();

function put(category, property, value, name) {
    var propertyMap = getMap(category, property);
    propertyMap.put(value, name);
}

function getMap(category, property) {

    var categoryMap = map.get(category);
    if (!categoryMap) {
        categoryMap = new base.Map();
        map.put(category, categoryMap);
    }

    var propertyMap = categoryMap.get(property);
    if (!propertyMap) {
        propertyMap = new base.Map();

        categoryMap.put(property, propertyMap);
    }
    return propertyMap;
}


dictionary.getName = function (category, property, value) {
    var categoryMap = map.get(category);
    var propertyMap = (categoryMap) ? categoryMap.get(property) : null;
    var name = (propertyMap) ? propertyMap.get(value) : '';
    return name;
};

dictionary.setName = function (list, category, valueProperty, nameProperty, children) {
    if (list) {
        var categoryMap = map.get(category);
        var propertyMap = (categoryMap) ? categoryMap.get(valueProperty) : null;

        var length = list.length;
        for (var i = 0; i < length; i++) {
            var item = list[i];
            var value = item[valueProperty];
            var name = (propertyMap) ? propertyMap.get(value) : '';
            item[nameProperty] = name;
            if (children && '' != children) {
                var nodes = item[children];
                if (nodes) {
                    dictionary.setName(nodes, category, valueProperty, nameProperty, children);
                }
            }
        }
    }
};

initMap();

function initMap () {
    put('article', 'status', '0', '草稿');
    put('article', 'status', '1', '已发表');
    put('article', 'status', '2', '回收站');

    put('articleCategory', 'type', '0', '未分类');
    put('articleCategory', 'type', '1', '广告');
    put('articleCategory', 'type', '2', '公告');
    put('articleCategory', 'type', '3', '资讯');

    put('common', 'flag', '0', '禁用');
    put('common', 'flag', '1', '启用');

    put('common', 'status', '0', '无效');
    put('common', 'status', '1', '有效');

    put('supply', 'deliveryMode', '1', '卖家包送');
    put('supply', 'deliveryMode', '2', '仓库自提');

    put('user', 'status', '0', '注册');
    put('user', 'status', '1', '审批中');
    put('user', 'status', '2', '有效');
    put('user', 'status', '3', '禁用');

    put('user', 'gender', '1', '男');
    put('user', 'gender', '2', '女');
    put('user', 'gender', '3', '保密');

    put('org', 'status', '0', '未认证');
    put('org', 'status', '1', '待审核');
    put('org', 'status', '2', '已认证');
    put('org', 'status', '3', '解约');

    put('org', 'nature', '0', '国有');
    put('org', 'nature', '1', '私企');
    put('org', 'nature', '2', '外资');
    put('org', 'nature', '3', '合资');

    put('org', 'isAuthorize', '0', '未认证');
    put('org', 'isAuthorize', '1', '已认证');
}

export default dictionary;
